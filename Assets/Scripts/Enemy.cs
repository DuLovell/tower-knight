﻿using System;
using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour, IHittable
{
    [SerializeField] private MeshRenderer _renderer;
    
    public void Hit(float damage)
    {
        StartCoroutine(HitRoutine());

        IEnumerator HitRoutine()
        {
            _renderer.material.color = Color.red;
            yield return new WaitForSeconds(0.2f);
            _renderer.material.color = Color.white;
        }
    }
}
