﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMover : MonoBehaviour
{
    [SerializeField] private float _duration = 0.5f;
    [SerializeField] private float _magnitute = 5f;
    
    private Rigidbody _rigidbody;

    public bool IsMoving { get; private set; }

    public void Move(Vector3 position)
    {
        StartCoroutine(MoveRoutine(position));

        IEnumerator MoveRoutine(Vector3 targetPosition)
        {
            IsMoving = true;
            
            Vector3 startPosition  = _rigidbody.position;
            float elapsedTime = 0;
         
            while (elapsedTime < _duration)
            {
                _rigidbody.position = Vector3.Lerp(startPosition, targetPosition, (elapsedTime / _duration));
                elapsedTime += Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }

            _rigidbody.position = targetPosition;

            IsMoving = false;
        }
    }
    
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
}
