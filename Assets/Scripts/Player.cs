using System;
using System.Collections;
using System.Collections.Generic;
using FSM.States.Player;
using UnityEngine;
using Utils;

[RequireComponent(typeof(PlayerMover))]
[RequireComponent(typeof(Shooter))]
public class Player : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private LayerMask _groundedLayers;
    
    [Header("Components")]
    [SerializeField] private CapsuleCollider _meleeAttackCollider;
    [SerializeField] private CapsuleCollider _aoeAttackCollider;

    [SerializeField] private Aimer _meleeAttackAimer;
    [SerializeField] private Aimer _rangeAttackAimer;

    private SwipeDetector _swipeDetector;
    private StateMachine _stateMachine;

    private PlayerMover _mover;
    private Shooter _shooter;

    private float _meleeDamage = 1f;
    private float _aoeAttackDuration = 0.2f;

    private const float OFFSET_ABOVE_PLAYER_LEGS = 0.5f;

    public Vector3 Position => transform.position;

    private void Awake()
    {
        _mover = GetComponent<PlayerMover>();
        _shooter = GetComponent<Shooter>();
    }

    private void Start()
    {
        _swipeDetector = SwipeDetector.Instance;
        _stateMachine = new StateMachine();

        // Определяем States
        IdleState idling = new IdleState();
        MeleeAttackAimingState meleeAttackAiming = new MeleeAttackAimingState(this, _meleeAttackAimer);
        MeleeAttackingState meleeAttacking = new MeleeAttackingState(this, _mover, _meleeAttackAimer, _meleeAttackCollider, _meleeDamage);
        AoeAttackPreparatingState aoeAttackPreparating = new AoeAttackPreparatingState();
        AoeAttackingState aoeAttacking = new AoeAttackingState(_aoeAttackCollider, _meleeDamage, _aoeAttackDuration);
        RangeAttackAimingState rangeAttackAiming = new RangeAttackAimingState(this, _rangeAttackAimer);
        RangeAttackingState rangeAttacking = new RangeAttackingState(this, _shooter, _rangeAttackAimer);
        DyingState dying = new DyingState();

        // Устанавливаем Transitions
        At(idling, meleeAttackAiming, InputQuickSwiped);
        At(idling, aoeAttackPreparating, InputTapHolded);
        At(idling, dying, IsNotGrounded);
        
        At(meleeAttackAiming, meleeAttacking, InputFingerReleased);
        At(meleeAttackAiming, dying, IsNotGrounded);
        
        At(meleeAttacking, idling, MeleeAttackEnded);
        
        At(aoeAttackPreparating, rangeAttackAiming, InputAfterHoldTapSwiped);
        At(aoeAttackPreparating, aoeAttacking, InputFingerReleased);
        At(aoeAttackPreparating, dying, IsNotGrounded);
        
        At(rangeAttackAiming, rangeAttacking, InputFingerReleased);
        At(rangeAttackAiming, dying, IsNotGrounded);
        
        At(rangeAttacking, idling, RangeAttackEnded);
        At(rangeAttacking, dying, IsNotGrounded);
        
        At(aoeAttacking, idling, AoeAttackEnded);
        At(aoeAttacking, dying, IsNotGrounded);


        _stateMachine.SetState(idling);

        void At(IState to, IState from, Func<bool> condition) => _stateMachine.AddTransition(to, from, condition);
        
        // Определяем Conditions
        bool InputQuickSwiped() => _swipeDetector.QuickSwipeStarted;
        bool InputTapHolded() => _swipeDetector.TapHolded;
        bool InputFingerReleased() => _swipeDetector.FingerReleased;
        bool InputAfterHoldTapSwiped() => _swipeDetector.SwipeAfterHoldTapStarted;
        bool MeleeAttackEnded() => !_mover.IsMoving;
        bool RangeAttackEnded() => true;
        bool AoeAttackEnded() => !aoeAttacking.IsAttacking;
        bool IsNotGrounded() => !GroundChecker.CheckGround(Position, _groundedLayers, OFFSET_ABOVE_PLAYER_LEGS);
    }

    private void Update() => _stateMachine.Tick();

    private void OnTriggerEnter(Collider other) => _stateMachine.OnTriggerEnter(other);
}
