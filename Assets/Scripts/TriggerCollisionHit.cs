﻿using System;
using UnityEngine;

public class TriggerCollisionHit : MonoBehaviour
{
    [SerializeField] private float _damage;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out IHittable hittable))
        {
            hittable.Hit(_damage);
        }
    }
}
