﻿using UnityEngine;

public interface IState
{
    void OnEnter();
    void Tick();

    void OnTriggerEnter(Collider other);

    void OnExit();
    
}
