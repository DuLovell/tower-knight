﻿using UnityEngine;

namespace FSM.States.Player
{
    public class RangeAttackingState : IState
    {
        private readonly global::Player _player;
        private readonly Shooter _shooter;
        private readonly Aimer _aimer;
        
        public RangeAttackingState(global::Player player, Shooter shooter, Aimer aimer)
        {
            _player = player;
            _shooter = shooter;
            _aimer = aimer;
        }
        
        public void OnEnter()
        {
            Debug.Log("Entered RangeAttacking State");
            _shooter.Shoot(_player.Position, _aimer.LastRevertedDirection);
        }
        
        public void Tick()
        {
        }

        public void OnTriggerEnter(Collider other)
        {
        }

        public void OnExit()
        {
        }
    }
}
