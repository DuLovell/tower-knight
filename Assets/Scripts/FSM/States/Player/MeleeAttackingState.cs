﻿using UnityEngine;

namespace FSM.States.Player
{
    public class MeleeAttackingState : IState
    {
        private readonly global::Player _player;
        private readonly PlayerMover _mover;
        private readonly Aimer _aimer;
        //TODO Заменить на SphereCast
        private readonly Collider _attackCollider;

        private readonly float _damage;

        public MeleeAttackingState(global::Player player, PlayerMover mover, Aimer aimer, Collider attackCollider, float damage)
        {
            _player = player;
            _mover = mover;
            _aimer = aimer;
            _attackCollider = attackCollider;

            _damage = damage;
        }
        
        public void OnEnter()
        {
            Debug.Log("Entered MeleeAttacking State");

            _attackCollider.enabled = true;
            Vector3 targetPosition = _player.Position + _aimer.LastRevertedDirection;
            _mover.Move(targetPosition);
        }
        
        public void Tick()
        {
        }

        public void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out IHittable hittable))
            {
                hittable.Hit(_damage);
            }
        }

        public void OnExit()
        {
            _attackCollider.enabled = false;
        }
    }
}
