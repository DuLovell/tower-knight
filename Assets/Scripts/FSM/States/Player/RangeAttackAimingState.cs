﻿using UnityEngine;

namespace FSM.States.Player
{
    public class RangeAttackAimingState : IState
    {
        private readonly global::Player _player;
        private readonly Aimer _aimer;

        public RangeAttackAimingState(global::Player player, Aimer aimer)
        {
            _player = player;
            _aimer = aimer;
        }
        
        public void OnEnter()
        {
            Debug.Log("Entered RangeAttackAiming State");
        }
        
        public void Tick()
        {
            _aimer.DrawRevertedTrajectoryFromInput(_player.Position);
        }

        public void OnTriggerEnter(Collider other)
        {
        }

        public void OnExit()
        {
            _aimer.HideTrajectory();
        }
    }
}
