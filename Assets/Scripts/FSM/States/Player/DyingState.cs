﻿using UnityEngine;

namespace FSM.States.Player
{
    public class DyingState : IState
    {
        public void OnEnter()
        {
            Debug.Log("Entered Dying State");
        }

        public void Tick()
        {
        }

        public void OnTriggerEnter(Collider other)
        {
        }

        public void OnExit()
        {
        }
    }
}
