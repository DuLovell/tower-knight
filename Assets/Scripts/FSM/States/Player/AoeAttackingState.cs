﻿using UnityEngine;
using Utils.SyncedTimer;

namespace FSM.States.Player
{
    public class AoeAttackingState : IState
    {
        private readonly Collider _attackCollider;
        private readonly SyncedTimer _attackDurationTimer;

        private readonly float _damage;
        private readonly float _duration;

        public bool IsAttacking => _attackDurationTimer.IsActive;
        
        public AoeAttackingState(Collider attackCollider, float damage, float duration)
        {
            _attackCollider = attackCollider;
            _attackDurationTimer = new SyncedTimer(TimerType.UpdateTick);
            
            _damage = damage;
            _duration = duration;
        }
        
        public void OnEnter()
        {
            Debug.Log("Entered AoeAttacking State");

            _attackCollider.enabled = true;
            _attackDurationTimer.Start(_duration);
        }
        
        public void Tick()
        {
            
        }

        public void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out IHittable hittable))
            {
                hittable.Hit(_damage);
            }
        }

        public void OnExit()
        {
            _attackCollider.enabled = false;
        }
    }
}
