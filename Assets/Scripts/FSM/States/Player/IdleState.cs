﻿using UnityEngine;

namespace FSM.States.Player
{
    public class IdleState : IState
    {
        public void Tick()
        {
        }

        public void OnTriggerEnter(Collider other)
        {
        }

        public void OnEnter()
        {
            Debug.Log("Entered Idle State");
        }

        public void OnExit()
        {
        }
    }
}
