﻿using UnityEngine;

namespace FSM.States.Player
{
    public class AoeAttackPreparatingState : IState
    {
        public void Tick()
        {
        }

        public void OnTriggerEnter(Collider other)
        {
        }

        public void OnEnter()
        {
            Debug.Log("Entered AoeAttackPreparating State");
        }

        public void OnExit()
        {
        }
    }
}
