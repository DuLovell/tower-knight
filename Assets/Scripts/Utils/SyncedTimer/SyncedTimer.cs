﻿// Автор: https://github.com/vavilichev/UnityUserful/tree/main/Assets/VavilichevGD/Utils/SyncedTimer 

using System;
using UnityEngine;

namespace Utils.SyncedTimer 
{
	public class SyncedTimer 
	{

		#region EVENTS

		public event Action<float> OnTimerValueChangedEvent;
		public event Action OnTimerFinishedEvent; 

		#endregion
		
		
		public TimerType Type { get; }
		public bool IsActive { get; private set; }
		public bool IsPaused { get; private set; }
		public float RemainingSeconds { get; private set; }
		

		public SyncedTimer(TimerType type) 
		{
			this.Type = type;
		}

		public SyncedTimer(TimerType type, float seconds) 
		{
			this.Type = type;
			
			SetTime(seconds);
		}

		
		public void SetTime(float seconds) 
		{
			RemainingSeconds = seconds;
			OnTimerValueChangedEvent?.Invoke(RemainingSeconds);
		}

		public void Start() 
		{
			if (IsActive)
				return;
			
			if (System.Math.Abs(RemainingSeconds) < Mathf.Epsilon) 
			{
#if DEBUG
				Debug.LogError("TIMER: You are trying start timer with remaining seconds equal 0.");
#endif
				OnTimerFinishedEvent?.Invoke();
			}

			IsActive = true;
			IsPaused = false;
			SubscribeOnTimeInvokerEvents();
			
			OnTimerValueChangedEvent?.Invoke(RemainingSeconds);
		}

		public void Start(float seconds) 
		{
			if (IsActive)
				return;
			
			SetTime(seconds);
			Start();
		}

		public void Pause() 
		{
			if (IsPaused || !IsActive)
				return;
			
			IsPaused = true;
			UnsubscribeFromTimeInvokerEvents();
			
			OnTimerValueChangedEvent?.Invoke(RemainingSeconds);
		}

		public void Unpause() 
		{
			if (!IsPaused || !IsActive)
				return;
			
			IsPaused = false;
			SubscribeOnTimeInvokerEvents();

			OnTimerValueChangedEvent?.Invoke(RemainingSeconds);
		}

		public void Stop() 
		{
			if (!IsActive)
				return;
			
			UnsubscribeFromTimeInvokerEvents();
			RemainingSeconds = 0f;
			IsActive = false;
			IsPaused = false;

			OnTimerValueChangedEvent?.Invoke(RemainingSeconds);
			OnTimerFinishedEvent?.Invoke();
		}

		
		private void SubscribeOnTimeInvokerEvents() 
		{
			switch (Type) 
			{
				case TimerType.UpdateTick:
					TimeInvoker.Instance.OnUpdateTimeTickedEvent += OnTicked;
					break;
				case TimerType.UpdateTickUnscaled:
					TimeInvoker.Instance.OnUpdateTimeUnscaledTickedEvent += OnTicked;
					break;
				case TimerType.OneSecTick:
					TimeInvoker.Instance.OnOneSyncedSecondTickedEvent += OnSyncedSecondTicked;
					break;
				case TimerType.OneSecTickUnscaled:
					TimeInvoker.Instance.OnOneSyncedSecondUnscaledTickedEvent += OnSyncedSecondTicked;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
		private void UnsubscribeFromTimeInvokerEvents()
		{
			switch (Type) 
			{
				case TimerType.UpdateTick:
					TimeInvoker.Instance.OnUpdateTimeTickedEvent -= OnTicked;
					break;
				case TimerType.UpdateTickUnscaled:
					TimeInvoker.Instance.OnUpdateTimeUnscaledTickedEvent -= OnTicked;
					break;
				case TimerType.OneSecTick:
					TimeInvoker.Instance.OnOneSyncedSecondTickedEvent -= OnSyncedSecondTicked;
					break;
				case TimerType.OneSecTickUnscaled:
					TimeInvoker.Instance.OnOneSyncedSecondUnscaledTickedEvent -= OnSyncedSecondTicked;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void CheckFinish() 
		{
			if (RemainingSeconds <= 0f) 
				Stop();
			else
				OnTimerValueChangedEvent?.Invoke(RemainingSeconds);
		}


		#region CALLBACKS

		private void OnTicked(float deltaTime) 
		{
			RemainingSeconds -= deltaTime;
			CheckFinish();
		}
		
		private void OnSyncedSecondTicked() 
		{
			RemainingSeconds -= 1;
			CheckFinish();
		}

		#endregion
	}
}
