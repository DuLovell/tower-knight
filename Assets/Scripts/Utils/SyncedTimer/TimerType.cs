﻿namespace Utils.SyncedTimer 
{
	public enum TimerType 
	{
		UpdateTick,
		UpdateTickUnscaled,
		OneSecTick,
		OneSecTickUnscaled
	}
}
