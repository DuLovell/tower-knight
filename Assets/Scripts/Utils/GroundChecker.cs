﻿using UnityEngine;

namespace Utils
{
    public class GroundChecker
    {
        public static bool CheckGround(Vector3 originPosition, LayerMask groundMask, float verticalOffset = 0f)
        {
            Vector3 rayStartPosition =
                new Vector3(originPosition.x, originPosition.y + verticalOffset, originPosition.z);
            Ray toGroundRay = new Ray(rayStartPosition, Vector3.down);
            return Physics.Raycast(toGroundRay, Mathf.Infinity, groundMask);
        }
    }
}
