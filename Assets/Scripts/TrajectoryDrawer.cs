﻿using System;
using UnityEngine;

public class TrajectoryDrawer
{
    private readonly LineRenderer _trajectoryRenderer;

    public TrajectoryDrawer(LineRenderer trajectoryRenderer)
    {
        _trajectoryRenderer = trajectoryRenderer;
    }
    
    public void DrawTrajectory(Vector3 startPosition, Vector3 direction)
    {
        _trajectoryRenderer.positionCount = 2;
        _trajectoryRenderer.SetPosition(0, startPosition);
        _trajectoryRenderer.SetPosition(1, startPosition + direction);
    }
}
