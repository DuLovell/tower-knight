﻿using System;
using UnityEngine;

public class TouchDetector : Singleton<TouchDetector>
{
    [SerializeField] private float _directionStrengthDivision = 40f;
    
    private Vector2 _startPosition;
    private Vector2 _currentPosition;

    public Vector2 CurrentMoveDirection => (_currentPosition - _startPosition) / _directionStrengthDivision;

    private void Update()
    {
        if (Input.touches.Length == 0) return;

        Touch primaryTouch = Input.touches[0];
        
        if (primaryTouch.phase == TouchPhase.Began)
        {
            _startPosition = primaryTouch.position;
            _currentPosition = _startPosition;
        }

        if (primaryTouch.phase == TouchPhase.Moved)
        {
            _currentPosition = primaryTouch.position;
        }

        if (primaryTouch.phase == TouchPhase.Ended)
        {
            _startPosition = Vector2.zero;
            _currentPosition = Vector2.zero;
        }
    }
}
