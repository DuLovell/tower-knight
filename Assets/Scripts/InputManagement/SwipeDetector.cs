﻿using System;
using UnityEngine;

public class SwipeDetector : Singleton<SwipeDetector>
{
    public static event Action<SwipeData> OnSwipe;
    
    [SerializeField] private bool _detectSwipeOnlyAfterRelease = false;
    [SerializeField] private float _minDistanceForSwipe = 100f;
    [SerializeField] private float _minHoldTapTime = 1f;
    
    private Vector2 _fingerEndPosition;
    private Vector2 _fingerStartPosition;

    private float _tapStartTime;

    public bool QuickSwipeStarted { get; private set; }
    public bool SwipeAfterHoldTapStarted { get; private set; }
    public bool TapHolded { get; private set; }
    public bool FingerReleased { get; private set; }

    protected override void Awake()
    {
        base.Awake();
        
        _tapStartTime = -1f;
    }

    private void Update()
    {
        TapHolded = _tapStartTime > 0f && (Time.time - _tapStartTime >= _minHoldTapTime);
        
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                FingerReleased = false;
                _tapStartTime = Time.time;
                
                _fingerStartPosition = touch.position;
                _fingerEndPosition = touch.position;
            }

            if (!_detectSwipeOnlyAfterRelease && touch.phase == TouchPhase.Moved)
            {
                if (QuickSwipeStarted == false && SwipeAfterHoldTapStarted == false)
                {
                    if (!TapHolded)
                    {
                        QuickSwipeStarted = true;
                    }
                    else
                    {
                        SwipeAfterHoldTapStarted = true;
                    }
                }
                
                _fingerEndPosition = touch.position;
                DetectSwipe();
            }

            if (touch.phase == TouchPhase.Ended)
            {
                FingerReleased = true;
                QuickSwipeStarted = false;
                SwipeAfterHoldTapStarted = false;
                TapHolded = false;
                _tapStartTime = -1f;
                
                _fingerEndPosition = touch.position;
                DetectSwipe();
            }
        }
    }

    private void DetectSwipe()
    {
        if (!SwipeDistanceCheckMet()) return;
        
        Vector2 direction = (_fingerEndPosition - _fingerStartPosition).normalized;
        SwipeDirection swipeDirection;
            
        if (IsVerticalSwipe())
        {
            swipeDirection = _fingerEndPosition.y - _fingerStartPosition.y > 0 ? SwipeDirection.Up : SwipeDirection.Down;
        }
        else
        {
            swipeDirection = _fingerEndPosition.x - _fingerStartPosition.x > 0 ? SwipeDirection.Right : SwipeDirection.Left;
        }
            
        SendSwipe(swipeDirection, direction);
        //_fingerStartPosition = _fingerEndPosition;
    }

    private bool IsVerticalSwipe()
    {
        return VerticalMovementDistance() > HorizontalMovementDistance();
    }

    private bool SwipeDistanceCheckMet()
    {
        return VerticalMovementDistance() > _minDistanceForSwipe || HorizontalMovementDistance() > _minDistanceForSwipe;
    }

    private float VerticalMovementDistance()
    {
        return Mathf.Abs(_fingerEndPosition.y - _fingerStartPosition.y);
    }

    private float HorizontalMovementDistance()
    {
        return Mathf.Abs(_fingerEndPosition.x - _fingerStartPosition.x);
    }

    private void SendSwipe(SwipeDirection swipeDirection, Vector2 direction)
    {
        SwipeData swipeData = new ()
        {
            SwipeDirection = swipeDirection,
            Direction = direction,
            StartPosition = _fingerEndPosition,
            EndPosition = _fingerStartPosition
        };
        
        OnSwipe?.Invoke(swipeData);
    }
}

public struct SwipeData
{
    public SwipeType Type;
    public Vector2 StartPosition;
    public Vector2 EndPosition;
    public SwipeDirection SwipeDirection;
    public Vector2 Direction;
}

public enum SwipeDirection
{
    Up,
    Down,
    Left,
    Right
}

public enum SwipeType
{
    Quick,
    AfterHold
}
