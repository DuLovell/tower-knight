﻿using Projectiles;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] private ConstantMover _projectileTemplate;

    public void Shoot(Vector3 shootPointPosition, Vector3 direction)
    {
        ConstantMover newProjectile = Instantiate(_projectileTemplate, shootPointPosition, Quaternion.identity);
        newProjectile.MoveInDirection(direction);
    }
}
