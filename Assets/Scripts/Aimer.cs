﻿using System;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Aimer : MonoBehaviour
{
    private LineRenderer _renderer;
    private TrajectoryDrawer _drawer;
    private TouchDetector _touchDetector;

    public Vector3 LastRevertedDirection { get; private set; }

    public void DrawRevertedTrajectoryFromInput(Vector3 startPosition)
    {
        _renderer.enabled = true;
        
        Vector3 revertedDirection = -1f * new Vector3(_touchDetector.CurrentMoveDirection.x, startPosition.y, _touchDetector.CurrentMoveDirection.y);
        _drawer.DrawTrajectory(startPosition, revertedDirection);

        LastRevertedDirection = revertedDirection;
    }

    public void HideTrajectory()
    {
        _renderer.enabled = false;
    }

    private void Awake()
    {
        _renderer = GetComponent<LineRenderer>();
        _drawer = new TrajectoryDrawer(_renderer);
    }

    private void Start()
    {
        _touchDetector = TouchDetector.Instance;
    }
}
