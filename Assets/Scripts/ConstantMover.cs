﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ConstantMover : MonoBehaviour
{
    private Rigidbody _rigidbody;

    private readonly WaitForFixedUpdate _waitForFixedUpdate = new WaitForFixedUpdate();

    public void MoveInDirection(Vector3 targetDirection)
    {
        StartCoroutine(MoveInDirectionRoutine(targetDirection));

        IEnumerator MoveInDirectionRoutine(Vector3 direction)
        {
            while (_rigidbody != null)
            {
                _rigidbody.velocity = direction;
                yield return _waitForFixedUpdate;
            }
        }
    }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
}
